package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

// TODO: complete me
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean toogle;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.toogle = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(toogle);
    }

    @Override
    public String chargedAttack() {
        toogle = !toogle;
        if(toogle) {
            return "Entered aim shot mode";
        }
        else {
            return "Leaving aim shot mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        //Complete
        return bow.getHolderName();
    }
}
