package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;


import java.util.List;

//Complete. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    //Implemented
    @Override
    public List<Weapon> findAll() {

        for(Bow bow : bowRepository.findAll()){
            if(weaponRepository.findByAlias(bow.getName()) == null) {
                weaponRepository.save(new BowAdapter(bow));
            }
        }

        for(Spellbook spellbook : spellbookRepository.findAll()){
            if(weaponRepository.findByAlias(spellbook.getName()) == null) {
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
        }

        return weaponRepository.findAll();
    }

    //Implemented
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        String tipeSerangan;
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if(attackType == 0) {
            tipeSerangan = "(normal attack) " + weapon.normalAttack();
        }
        else if(attackType == 1) {
            tipeSerangan = "(charged attack) " + weapon.chargedAttack();
        }
        else {
            tipeSerangan = "Belum ada";
        }
        String log = weapon.getHolderName() + " attacked with " + weapon.getName() + " " + tipeSerangan;
        logRepository.addLog(log);
        weaponRepository.save(weapon);
    }

    //Implemented
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
