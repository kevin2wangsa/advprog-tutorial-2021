package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

//Complete :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean deteksiChargedBerulang;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.deteksiChargedBerulang = false;
    }

    @Override
    public String normalAttack() {
        deteksiChargedBerulang = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(!deteksiChargedBerulang) {
            deteksiChargedBerulang = true;
            return spellbook.largeSpell();
        }
        else {
            deteksiChargedBerulang = false;
            return "Magic power not enough for large spell";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}

