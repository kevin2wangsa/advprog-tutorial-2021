package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {

    public Spell encode (Spell spell) {
        return process(spell, true);
    }

    public Spell decode (Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int shift = encode ? 1 : -1;
        int n = text.length();
        char[] res = new char[n];

        for(int i = 0; i < n; i++){
            int asciiCharNow = (int)text.charAt(i);
            int asciiCharNew = asciiCharNow + shift;
            if (asciiCharNow == 32) {
                asciiCharNew = asciiCharNow;
            }
            if (asciiCharNew == 123 || asciiCharNew == 91) {
                asciiCharNew -= 26;
            } else if (asciiCharNew == 96 || asciiCharNew == 64) {
                asciiCharNew += 26;
            } else if (asciiCharNew == 58) {
                asciiCharNew -= 10;
            } else if (asciiCharNew == 47) {
                asciiCharNew += 10;
            }
            res[i] = (char)asciiCharNew;
        }


        return new Spell(new String(res), codex);
    }
}
