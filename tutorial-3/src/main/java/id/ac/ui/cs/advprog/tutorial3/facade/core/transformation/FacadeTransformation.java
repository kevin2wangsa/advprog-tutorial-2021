package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class FacadeTransformation {

    AbyssalTransformation abyss;
    CelestialTransformation celes;
    Codex runic;
    Codex alpha;

    public FacadeTransformation() {
        abyss = new AbyssalTransformation();
        celes = new CelestialTransformation();
        runic = RunicCodex.getInstance();
        alpha = AlphaCodex.getInstance();
    }

    public String encode(String text) {
        Spell step01 = new Spell(text, alpha);
        Spell step02 = abyss.encode(step01);
        Spell step03 = celes.encode(step02);
        Spell step04 = CodexTranslator.translate(step03, runic);
        return step04.getText();
    }

    public String decode(String code) {
        Spell step01 = new Spell(code, runic);
        Spell step02 = CodexTranslator.translate(step01, alpha);
        Spell step03 = celes.decode(step02);
        Spell step04 = abyss.decode(step03);
        return step04.getText();
    }

}
