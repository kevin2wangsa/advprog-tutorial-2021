package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CaesarTransformationTest {
    private Class<?> caesarClass;

    @BeforeEach
    public void setup() throws Exception {
        caesarClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation");
    }

    @Test
    public void testCaesarHasEncodeMethod() throws Exception {
        Method translate = caesarClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarHasDecodeMethod() throws Exception {
        Method translate = caesarClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarEncodesCorrectly() throws Exception {
        String text = "Ini sebuah test";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expect = "Joj tfcvbi uftu";

        Spell result = new CaesarTransformation().encode(spell);
        assertEquals(expect, result.getText());
    }

    @Test
    public void testCaesarDecodesCorrectly() throws Exception {
        String text = "Joj tfcvbi uftu";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expect = "Ini sebuah test";

        Spell result = new CaesarTransformation().decode(spell);
        assertEquals(expect, result.getText());
    }
}

