package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.List;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    private List<Spell> spells;
    private int len;

    public ChainSpell(List<Spell> spells) {
        this.spells = spells;
        len = spells.size();
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        // cast all but how
        for (int i = 0; i < len; i++) {
            spells.get(i).cast();
        }
    }

    @Override
    public void undo() {
        // hoooow
        for (int i = len-1; i >= 0; i--) {
            spells.get(i).undo();
        }
    }
}
