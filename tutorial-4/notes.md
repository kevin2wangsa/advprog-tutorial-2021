# Lazy Instantiation

Lazy Instantiation adalah cara yang digunakan untuk menunda pembuatan objek pada program. 

Keuntungan
- Meningkatkan performa atau kinerja
- Menghindari perhitungan atau tenaga komputasi yang boros
- Mengurangi penggunaan program memory

Kerugian
- Sulit untuk menangani kasus multithreading
- Method getInstance() jadi lebih panjang karena ada pengecekan apakah objek sudah ada atau belum berulang kali

# Eager Instantiation

Eager instantiation adalah taktik untuk langsung membuat objek walaupun objek tersebut belum dibutuhkan dan merupakan kebalikan dari lazy

Keuntungan
- Multithreading ditangani dengan mudah
- Method getInstance() jadi lebih singkat

Kerugian:

- Penggunaan memori yang boros.
- Menurunkan performa code.
