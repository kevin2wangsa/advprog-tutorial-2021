package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

public class LiyuanSoba extends Menu {
    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet
    public LiyuanSoba(String name){
        super(name, new LiyuanSobaProductsFactory());
    }
}