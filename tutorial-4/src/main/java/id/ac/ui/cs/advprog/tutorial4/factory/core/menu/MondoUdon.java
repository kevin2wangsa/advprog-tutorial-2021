package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

public class MondoUdon extends Menu {
    //Ingridients:
    //Noodle: Udon
    //Meat: Chicken
    //Topping: Cheese
    //Flavor: Salty
    public MondoUdon(String name){

        super(name,new MondoUdonProductsFactory());

    }
}