package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

public class SnevnezhaShirataki extends Menu {
    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami
    public SnevnezhaShirataki(String name){
        super(name , new SnevnezhaShiratakiProductsFactory());
    }
}