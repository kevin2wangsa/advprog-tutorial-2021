package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;

//Ingridients:
//Noodle: Shirataki
//Meat: Fish
//Topping: Flower
//Flavor: Umami

public class SnevnezhaShiratakiProductsFactory implements IngridientsFactory{

    @Override
    public Noodle createNoodle(){
        return new Shirataki();
    }

    @Override
    public Meat createMeat(){
        return new Fish();
    }

    @Override
    public Topping createTopping(){
        return new Flower();
    }

    @Override
    public Flavor createFlavor(){
        return new Umami();
    }

}
