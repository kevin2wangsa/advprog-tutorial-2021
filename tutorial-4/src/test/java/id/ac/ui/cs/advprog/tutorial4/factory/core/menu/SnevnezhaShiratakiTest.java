package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;
    private SnevnezhaShirataki snevnezhaShirataki;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
        snevnezhaShirataki = new SnevnezhaShirataki("Test");
    }

    @Test
    public void testSnevnezhaShiratakiIsAPublicClass() {
        int classModifiers = snevnezhaShiratakiClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiGetNameShouldReturnName() {
        assertEquals("Test", snevnezhaShirataki.getName());
    }

    @Test
    public void testSnevnezhaShiratakiGetNoodleShouldReturnShirataki() {
        Noodle noodle = snevnezhaShirataki.getNoodle();
        assertThat(noodle).isInstanceOf(Shirataki.class);
    }

    @Test
    public void testSnevnezhaShiratakiGetMeatShouldReturnFish() {
        Meat meat = snevnezhaShirataki.getMeat();
        assertThat(meat).isInstanceOf(Fish.class);
    }

    @Test
    public void testSnevnezhaShiratakiGetToppingShouldReturnFlower() {
        Topping topping = snevnezhaShirataki.getTopping();
        assertThat(topping).isInstanceOf(Flower.class);
    }

    @Test
    public void testSnevnezhaShiratakiGetFlavorShouldReturnUmami() {
        Flavor flavor = snevnezhaShirataki.getFlavor();
        assertThat(flavor).isInstanceOf(Umami.class);
    }

}
