package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;



public class BoiledEggTest {
    private Class<?> boiledEggClass;

    @BeforeEach
    public void setup() throws Exception {
        boiledEggClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
    }

    @Test
    public void testBoiledEggIsConcreteClass() {
        int classModifiers = boiledEggClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testBoiledEggIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(boiledEggClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testBoiledEggGetDescriptionShouldReturnDescription() throws Exception {
        BoiledEgg BoiledEgg = new BoiledEgg();
        assertEquals("Adding Guahuan Boiled Egg Topping", BoiledEgg.getDescription());
    }

}
