package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

public class BeefTest {
    private Class<?> beefClass;

    @BeforeEach
    public void setup() throws Exception {
        beefClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef");
    }

    @Test
    public void testBeefIsConcreteClass() {
        int classModifiers = beefClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testBeefIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(beefClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testBeefGetDescriptionShouldReturnDescription() throws Exception {
        Beef Beef = new Beef();
        assertEquals("Adding Maro Beef Meat...", Beef.getDescription());
    }
}
