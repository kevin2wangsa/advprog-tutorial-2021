package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderFoodTest {

    @Test
    public void testOrderFoodOnlyCreatedOnce() {
        OrderFood orderFoodTest1 = OrderFood.getInstance();
        OrderFood orderFoodTest2 = OrderFood.getInstance();

        assertThat(orderFoodTest1).isEqualToComparingFieldByField(orderFoodTest2);
    }

}
