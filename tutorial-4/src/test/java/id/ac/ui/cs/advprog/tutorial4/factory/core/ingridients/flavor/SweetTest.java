package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

public class SweetTest {
    private Class<?> sweetClass;

    @BeforeEach
    public void setup() throws Exception {
        sweetClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet");
    }

    @Test
    public void testSweetIsConcreteClass() {
        int classModifiers = sweetClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testSweetIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(sweetClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testSweetGetDescriptionShouldReturnDescription() throws Exception {
        Sweet Sweet = new Sweet();
        assertEquals("Adding a dash of Sweet Soy Sauce...", Sweet.getDescription());
    }

}

