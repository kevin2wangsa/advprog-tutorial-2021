package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class LiyuanSobaProductsFactoryTest {
    private Class<?> LiyuanSobaProductsFactoryClass;
    private LiyuanSobaProductsFactory LiyuanSobaProductsFactory;

    @BeforeEach
    public void setUp() throws Exception {
        LiyuanSobaProductsFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSobaProductsFactory");
        LiyuanSobaProductsFactory = new LiyuanSobaProductsFactory();
    }

    @Test
    public void testliyuanSobaIngridientsFactoryTestIsConcreteClass() {
        int classModifiers = LiyuanSobaProductsFactoryClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryIsAMenuIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(LiyuanSobaProductsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngridientsFactory")));
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = LiyuanSobaProductsFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = LiyuanSobaProductsFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = LiyuanSobaProductsFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = LiyuanSobaProductsFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryCreateNoodleShouldReturnSoba() throws Exception {
        Noodle noodle = LiyuanSobaProductsFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Soba.class);
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryCreateMeatShouldReturnBeef() throws Exception {
        Meat meat = LiyuanSobaProductsFactory.createMeat();
        assertThat(meat).isInstanceOf(Beef.class);
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryCreateToppingShouldReturnMushroom() throws Exception {
        Topping topping = LiyuanSobaProductsFactory.createTopping();
        assertThat(topping).isInstanceOf(Mushroom.class);
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryCreateFlavorShouldReturnSweet() throws Exception {
        Flavor flavor = LiyuanSobaProductsFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Sweet.class);
    }

}
