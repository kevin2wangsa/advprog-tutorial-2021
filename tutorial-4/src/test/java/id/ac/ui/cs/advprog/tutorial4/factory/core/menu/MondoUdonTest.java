package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;
    private MondoUdon mondoUdon;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        mondoUdon = new MondoUdon("Test");
    }

    @Test
    public void testMondoUdonIsAPublicClass() {
        int classModifiers = mondoUdonClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testMondoUdonIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonGetNameShouldReturnName() {
        assertEquals("Test", mondoUdon.getName());
    }

    @Test
    public void testMondoUdonGetNoodleShouldReturnUdon() {
        Noodle noodle = mondoUdon.getNoodle();
        assertThat(noodle).isInstanceOf(Udon.class);
    }

    @Test
    public void testMondoUdonGetMeatShouldReturnChicken() {
        Meat meat = mondoUdon.getMeat();
        assertThat(meat).isInstanceOf(Chicken.class);
    }

    @Test
    public void testMondoUdonGetToppingShouldReturnCheese() {
        Topping topping = mondoUdon.getTopping();
        assertThat(topping).isInstanceOf(Cheese.class);
    }

    @Test
    public void testMondoUdonGetFlavorShouldReturnSalty() {
        Flavor flavor = mondoUdon.getFlavor();
        assertThat(flavor).isInstanceOf(Salty.class);
    }

}
