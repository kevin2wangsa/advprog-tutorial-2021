package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class UmamiTest {
    private Class<?> umamiClass;

    @BeforeEach
    public void setup() throws Exception {
        umamiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
    }

    @Test
    public void testUmamiIsConcreteClass() {
        int classModifiers = umamiClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testUmamiIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(umamiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testUmamiGetDescriptionShouldReturnDescription() throws Exception {
        Umami Umami = new Umami();
        assertEquals("Adding WanPlus Specialty MSG flavoring...", Umami.getDescription());
    }
}
