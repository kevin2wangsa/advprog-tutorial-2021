package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @BeforeEach
    public void setUp() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService");
    }

    @Test
    public void testMenuServiceIsAPublicInterface() {
        int classModifiers = menuServiceClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMenuServiceHasGetMenusAbstractMethodAndCount() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getMenus.getParameterCount());
    }

    @Test
    public void testMenuServiceHasCreateMenuAbstractMethodAndCount() throws Exception {
        Class<?>[] createMenuArgs = new Class[2];
        createMenuArgs[0] = String.class;
        createMenuArgs[1] = String.class;
        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", createMenuArgs);
        int methodModifiers = createMenu.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(2, createMenu.getParameterCount());
    }
}
