package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;


public class UdonTest {
    private Class<?> udonClass;

    @BeforeEach
    public void setup() throws Exception {
        udonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon");
    }

    @Test
    public void testUdonIsConcreteClass() {
        int classModifiers = udonClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testUdonIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(udonClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testUdonGetDescriptionShouldReturnDescription() throws Exception {
        Udon Udon = new Udon();
        assertEquals("Adding Mondo Udon Noodles...", Udon.getDescription());
    }
}
