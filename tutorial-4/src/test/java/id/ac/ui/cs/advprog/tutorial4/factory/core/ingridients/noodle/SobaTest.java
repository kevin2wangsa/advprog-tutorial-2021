package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;


public class SobaTest {
    private Class<?> sobaClass;

    @BeforeEach
    public void setup() throws Exception {
        sobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba");
    }

    @Test
    public void testSobaIsConcreteClass() {
        int classModifiers = sobaClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testSobaIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(sobaClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testSobaGetDescriptionShouldReturnDescription() throws Exception {
        Soba Soba = new Soba();
        assertEquals("Adding Liyuan Soba Noodles...", Soba.getDescription());
    }
}

