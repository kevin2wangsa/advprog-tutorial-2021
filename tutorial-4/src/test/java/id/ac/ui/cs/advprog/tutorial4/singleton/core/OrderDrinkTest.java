package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderDrinkTest {

    @Test
    public void testOrderDrinkOnlyCreatedOnce() {
        OrderDrink orderDrinkTest1 = OrderDrink.getInstance();
        OrderDrink orderDrinkTest2 = OrderDrink.getInstance();

        assertThat(orderDrinkTest1).isEqualToComparingFieldByField(orderDrinkTest2);
    }

}
