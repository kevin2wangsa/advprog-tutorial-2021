package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;


public class PorkTest {
    private Class<?> porkClass;

    @BeforeEach
    public void setup() throws Exception {
        porkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork");
    }

    @Test
    public void testPorkIsConcreteClass() {
        int classModifiers = porkClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testPorkIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(porkClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testPorkGetDescriptionShouldReturnDescription() throws Exception {
        Pork Pork = new Pork();
        assertEquals("Adding Tian Xu Pork Meat...", Pork.getDescription());
    }
}
