package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;
    private InuzumaRamen inuzumaRamen;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        inuzumaRamen = new InuzumaRamen("Test");
    }

    @Test
    public void testInuzumaRamenIsAPublicClass() {
        int classModifiers = inuzumaRamenClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testInuzumaRamenIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenGetNameShouldReturnName() {
        assertEquals("Test", inuzumaRamen.getName());
    }

    @Test
    public void testInuzumaRamenGetNoodleShouldReturnRamen() {
        Noodle noodle = inuzumaRamen.getNoodle();
        assertThat(noodle).isInstanceOf(Ramen.class);
    }

    @Test
    public void testInuzumaRamenGetMeatShouldReturnPork() {
        Meat meat = inuzumaRamen.getMeat();
        assertThat(meat).isInstanceOf(Pork.class);
    }

    @Test
    public void testInuzumaRamenGetToppingShouldReturnBoiledEgg() {
        Topping topping = inuzumaRamen.getTopping();
        assertThat(topping).isInstanceOf(BoiledEgg.class);
    }

    @Test
    public void testInuzumaRamenGetFlavorShouldReturnSpicy() {
        Flavor flavor = inuzumaRamen.getFlavor();
        assertThat(flavor).isInstanceOf(Spicy.class);
    }

}
