package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;


public class RamenTest {
    private Class<?> ramenClass;

    @BeforeEach
    public void setup() throws Exception {
        ramenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen");
    }

    @Test
    public void testRamenIsConcreteClass() {
        int classModifiers = ramenClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testRamenIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(ramenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testRamenGetDescriptionShouldReturnDescription() throws Exception {
        Ramen Ramen = new Ramen();
        assertEquals("Adding Inuzuma Ramen Noodles...", Ramen.getDescription());
    }

}

