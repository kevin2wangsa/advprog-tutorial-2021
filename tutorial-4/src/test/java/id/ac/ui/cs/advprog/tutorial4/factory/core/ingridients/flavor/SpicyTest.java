package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;


public class SpicyTest {
    private Class<?> spicyClass;

    @BeforeEach
    public void setup() throws Exception {
        spicyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy");
    }

    @Test
    public void testSpicyIsConcreteClass() {
        int classModifiers = spicyClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testSpicyIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(spicyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testSpicyGetDescriptionShouldReturnDescription() throws Exception {
        Spicy Spicy = new Spicy();
        assertEquals("Adding Liyuan Chili Powder...", Spicy.getDescription());
    }

}
