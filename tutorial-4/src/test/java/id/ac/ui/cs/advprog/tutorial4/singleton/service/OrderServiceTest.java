package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Spy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class OrderServiceTest {

    @Mock
    OrderDrink orderDrink;

    @Mock
    OrderFood orderFood;

    @Spy
    OrderServiceImpl orderService = new OrderServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        orderDrink = mock(OrderDrink.class);
        orderFood = mock(OrderFood.class);
    }

    @Test
    public void testOrderServiceOrderADrinkImplementedCorrectly() throws Exception {
        String drink = "Test";
        doNothing().when(orderDrink).setDrink(isA(String.class));
        when(orderDrink.getDrink()).thenReturn(drink);

        orderService.orderADrink("Test");
        assertEquals("Test", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderServiceOrderAFoodImplementedCorrectly() throws Exception {
        String food = "Test";
        doNothing().when(orderFood).setFood(isA(String.class));
        when(orderFood.getFood()).thenReturn(food);

        orderService.orderAFood("Test");
        assertEquals("Test", orderService.getFood().getFood());
    }
}
