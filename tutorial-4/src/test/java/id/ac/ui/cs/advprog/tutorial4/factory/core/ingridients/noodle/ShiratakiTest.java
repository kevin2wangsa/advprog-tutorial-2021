package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;


public class ShiratakiTest {
    private Class<?> shiratakiClass;

    @BeforeEach
    public void setup() throws Exception {
        shiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki");
    }

    @Test
    public void testShiratakiIsConcreteClass() {
        int classModifiers = shiratakiClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testShiratakiIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(shiratakiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testShiratakiGetDescriptionShouldReturnDescription() throws Exception {
        Shirataki Shirataki = new Shirataki();
        assertEquals("Adding Snevnezha Shirataki Noodles...", Shirataki.getDescription());
    }

}
