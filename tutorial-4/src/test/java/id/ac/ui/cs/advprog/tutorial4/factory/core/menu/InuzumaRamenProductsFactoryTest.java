package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenProductsFactoryTest {
    private Class<?> InuzumaRamenIProductsFactoryClass;
    private InuzumaRamenProductsFactory InuzumaRamenIProductsFactory;

    @BeforeEach
    public void setUp() throws Exception {
        InuzumaRamenIProductsFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamenProductsFactory");
        InuzumaRamenIProductsFactory = new InuzumaRamenProductsFactory();
    }

    @Test
    public void testinuzumaRamenIngridientsFactoryClassIsConcreteClass() {
        int classModifiers = InuzumaRamenIProductsFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryIsAMenuIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(InuzumaRamenIProductsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngridientsFactory")));
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = InuzumaRamenIProductsFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = InuzumaRamenIProductsFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = InuzumaRamenIProductsFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = InuzumaRamenIProductsFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryCreateNoodleShouldReturnRamen() throws Exception {
        Noodle noodle = InuzumaRamenIProductsFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Ramen.class);
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryCreateMeatShouldReturnPork() throws Exception {
        Meat meat = InuzumaRamenIProductsFactory.createMeat();
        assertThat(meat).isInstanceOf(Pork.class);
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryCreateToppingShouldReturnBoiledEgg() throws Exception {
        Topping topping = InuzumaRamenIProductsFactory.createTopping();
        assertThat(topping).isInstanceOf(BoiledEgg.class);
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryCreateFlavorShouldReturnSpicy() throws Exception {
        Flavor flavor = InuzumaRamenIProductsFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Spicy.class);
    }

}
