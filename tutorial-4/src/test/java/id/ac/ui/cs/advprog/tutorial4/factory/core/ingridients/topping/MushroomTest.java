package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

public class MushroomTest {
    private Class<?> mushroomClass;

    @BeforeEach
    public void setup() throws Exception {
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
    }

    @Test
    public void testMushroomIsConcreteClass() {
        int classModifiers = mushroomClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMushroomIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(mushroomClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testMushroomGetDescriptionShouldReturnDescription() throws Exception {
        Mushroom Mushroom = new Mushroom();
        assertEquals("Adding Shiitake Mushroom Topping...", Mushroom.getDescription());
    }

}
