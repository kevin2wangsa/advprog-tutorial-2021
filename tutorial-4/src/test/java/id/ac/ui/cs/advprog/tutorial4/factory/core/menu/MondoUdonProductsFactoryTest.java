package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonProductsFactoryTest {
    private Class<?> MondoUdonProductsFactoryClass;
    private MondoUdonProductsFactory MondoUdonProductsFactory;

    @BeforeEach
    public void setUp() throws Exception {
        MondoUdonProductsFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdonProductsFactory");
        MondoUdonProductsFactory = new MondoUdonProductsFactory();
    }

    @Test
    public void testmondoUdonIngridientsFactoryTestIsConcreteClass() {
        int classModifiers = MondoUdonProductsFactoryClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMondoUdonIngridientsFactoryIsAMenuIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(MondoUdonProductsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngridientsFactory")));
    }

    @Test
    public void testMondoUdonIngridientsFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = MondoUdonProductsFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngridientsFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = MondoUdonProductsFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngridientsFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = MondoUdonProductsFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngridientsFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = MondoUdonProductsFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngridientsFactoryCreateNoodleShouldReturnUdon() throws Exception {
        Noodle noodle = MondoUdonProductsFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Udon.class);
    }

    @Test
    public void testMondoUdonIngridientsFactoryCreateMeatShouldReturnChicken() throws Exception {
        Meat meat = MondoUdonProductsFactory.createMeat();
        assertThat(meat).isInstanceOf(Chicken.class);
    }

    @Test
    public void testMondoUdonIngridientsFactoryCreateToppingShouldReturnCheese() throws Exception {
        Topping topping = MondoUdonProductsFactory.createTopping();
        assertThat(topping).isInstanceOf(Cheese.class);
    }

    @Test
    public void testMondoUdonIngridientsFactoryCreateFlavorShouldReturnSalty() throws Exception {
        Flavor flavor = MondoUdonProductsFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Salty.class);
    }

}
