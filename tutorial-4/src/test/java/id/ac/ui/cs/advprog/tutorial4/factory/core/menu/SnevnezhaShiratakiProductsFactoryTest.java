package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiProductsFactoryTest {
    private Class<?> SnevnezhaShiratakiProductsFactoryClass;
    private SnevnezhaShiratakiProductsFactory SnevnezhaShiratakiProductsFactory;

    @BeforeEach
    public void setUp() throws Exception {
        SnevnezhaShiratakiProductsFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShiratakiProductsFactory");
        SnevnezhaShiratakiProductsFactory = new SnevnezhaShiratakiProductsFactory();
    }

    @Test
    public void testsnevnezhaShiratakiIngridientsFactoryTestIsConcreteClass() {
        int classModifiers = SnevnezhaShiratakiProductsFactoryClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryIsAMenuIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(SnevnezhaShiratakiProductsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngridientsFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = SnevnezhaShiratakiProductsFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = SnevnezhaShiratakiProductsFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = SnevnezhaShiratakiProductsFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = SnevnezhaShiratakiProductsFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryCreateNoodleShouldReturnShirataki() throws Exception {
        Noodle noodle = SnevnezhaShiratakiProductsFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Shirataki.class);
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryCreateMeatShouldReturnFish() throws Exception {
        Meat meat = SnevnezhaShiratakiProductsFactory.createMeat();
        assertThat(meat).isInstanceOf(Fish.class);
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryCreateToppingShouldReturnFlower() throws Exception {
        Topping topping = SnevnezhaShiratakiProductsFactory.createTopping();
        assertThat(topping).isInstanceOf(Flower.class);
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryCreateFlavorShouldReturnUmami() throws Exception {
        Flavor flavor = SnevnezhaShiratakiProductsFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Umami.class);
    }

}
