package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;


public class ChickenTest {
    private Class<?> chickenClass;

    @BeforeEach
    public void setup() throws Exception {
        chickenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
    }

    @Test
    public void testChickenIsConcreteClass() {
        int classModifiers = chickenClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testChickenIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(chickenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testChickenGetDescriptionShouldReturnDescription() throws Exception {
        Chicken Chicken = new Chicken();
        assertEquals("Adding Wintervale Chicken Meat...", Chicken.getDescription());
    }
}
